#!/bin/bash

SCRIPTPATH=$(realpath $0)
SCRIPTDIR=$(dirname $SCRIPTPATH)
cd $SCRIPTDIR/

sudo mount -t sysfs sys ./root/sys/
sudo mount -t proc proc ./root/proc/
sudo mount --bind /dev ./root/dev/
sudo mount --bind /dev/pts ./root/dev/pts/
sudo mount --bind /dev/shm ./root/dev/shm/
