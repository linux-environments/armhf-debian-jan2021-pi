#!/bin/bash

SCRIPTPATH=$(realpath $0)
SCRIPTDIR=$(dirname $SCRIPTPATH)
cd $SCRIPTDIR/

./mountdevs.sh

sudo chroot ./root/ /bin/su -l pi

./umountdevs.sh
