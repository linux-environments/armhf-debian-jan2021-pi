#!/bin/bash

SCRIPTPATH=$(realpath $0)
SCRIPTDIR=$(dirname $SCRIPTPATH)
cd $SCRIPTDIR/

sleep 10

./mountdevs.sh

sudo chroot ./root/ /bin/su -l pi /home/pi/startup.sh

./umountdevs.sh
