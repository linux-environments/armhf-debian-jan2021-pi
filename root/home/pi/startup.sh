#!/bin/bash

SCRIPTPATH=$(realpath $0)
SCRIPTDIR=$(dirname $SCRIPTPATH)
cd $SCRIPTDIR/

# Put stuff to run at chroot startup here.
# Example:
# ./nicescript.sh &
# python3 ./epicscript.py
